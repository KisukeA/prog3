import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Objects;
import java.util.Scanner;

public class TicTacToe implements Serializable {
    //For now its tic-tac-toe but I might change it later
    public int playerXO = 0;
    public boolean Xmoved=false;
    public boolean Omoved=false;
    public String[][] map = {{"▢","▢","▢"},{"▢","▢","▢"},{"▢","▢","▢"}};
    public int winner = -1;
    int one;
    int two;
    public PrintWriter pw;
    public BufferedReader br;
    Scanner s;
    TicTacToe(PrintWriter pw, BufferedReader br){
        this.pw = pw;
        this.br = br;
        //s = new Scanner(System.in);
        /*this.map = new String[3][3];
        for(int i = 0; i < 3; i++){
            Arrays.fill(map[i],"▢");
        }*/
        System.out.println("we here babyyy");
    }
    public void move(int x, int y, String s){
        /*if(s=="〇" && playerXO==0){
            System.out.println("Wait, it's not your turn");
        }
        if(s=="⨉" && playerXO==1){
            System.out.println("Wait, it's not your turn");
        }*/
        if(isValid(x,y)){
            map[x][y] = s;
        }
        playerXO=(playerXO+1)%2;
        System.out.println("changed playerxo to "+playerXO);
        if(s.equals("〇")){
            Omoved = true;
        }
        else {
            Xmoved = true;
        }
        if(isWinner(s)&&s.equals("〇")){
            this.winner = 2;
        }
        else if(isWinner(s)&&s.equals("⨉")){
            this.winner = 1;
        }
        else if(draw()){
            this.winner = 0;
        }
    }

    private boolean draw() {
        for(int i = 0; i<3; i++){
            for(int j =0; j<3; j++){
                if (map[i][j].equals("▢")){
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isValid(int x, int y) {
        if (map[x][y].equalsIgnoreCase("▢") ) {
            return true;
        }

        return false;
    }
    public boolean isWinner(String s){
        if(map[0][0].equals(s) && map[0][1].equals(s) && map[0][2].equals(s)) return true;
        else if(map[1][0].equals(s) && map[1][1].equals(s) && map[1][2].equals(s)) return true;
        else if(map[2][0].equals(s) && map[2][1].equals(s) && map[2][2].equals(s)) return true;
        else if(map[0][0].equals(s) && map[1][0].equals(s) && map[2][0].equals(s)) return true;
        else if(Objects.equals(map[0][1], s) && map[1][1].equals(s) && map[2][1].equals(s)) return true;
        else if(map[0][2].equals(s) && map[1][2].equals(s) && map[2][2].equals(s)) return true;
        else if(map[0][0].equals(s) && map[1][1].equals(s) && map[2][2].equals(s)) return true;
        else if(map[2][0].equals(s) && map[1][1].equals(s) && map[0][2].equals(s)) return true;
        else return false;
    }
    public void gameState() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
    }
    /*public void start(){
        System.out.println("//enter the coordinates as a two-digit number, the tens place is for x coord. ones is for y");
        System.out.println("//x increases going down, y increases going right, both starting from 1");
        System.out.println("//the top leftmost position is 11");
        gameState();
        int id = 0;
        int x;
        int y;
        while(!isWinner(id%2==1?"⨉":"〇") && id<9){
            do{
                System.out.println("Player " + ((id%2)+1) + ", please enter your move: ");
                x = s.nextInt();
                y = x%10 - 1;
                x/=10;
                x--;
            }while(x<0 || x>2 || y<0 || y>2 || !isValid(x,y));
            move(x,y,id%2==0?"⨉":"〇");
            gameState();
            id++;
        }
        System.out.println("GG NOOBS");
    }*/
    public static void main(String[] args){
        //TicTacToe game = new TicTacToe();
        //game.start();
    }
    public String toString(){

        return null;
    }
}
