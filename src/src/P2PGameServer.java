import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class P2PGameServer {
    public static final int PORT = 8888;
    private static int getPort=8888;
    public static ArrayList<P2PClientHandler> handlers;
    public static void main(String[] args) throws IOException {
        PeerManager pm = new PeerManager();
        //connectedPeers = new ArrayList<>();
        handlers = new ArrayList<>(0);
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            System.out.println("Server started on port " + PORT);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                //System.out.println("New client connected: " + clientSocket.getInetAddress().getHostAddress());
                InetSocketAddress ins = (InetSocketAddress)clientSocket.getRemoteSocketAddress();
                InetAddress inad = ins.getAddress();
                int inp = ins.getPort();
                System.out.println("New client connected: " + inad);
                System.out.println("New client connected: " + inp);
                System.out.println("New client connected: " + clientSocket.getLocalAddress());
                System.out.println("New client connected: " + ins);

                // Handle client connection in a new thread
                //pm.addPeer(new Peer(clientSocket));
                //connectedPeers.add(clientSocket.getInetAddress().getHostAddress());
                P2PClientHandler handler = new P2PClientHandler(clientSocket);
                handlers.add(handler);
                Thread clientThread = new Thread(handler);
                clientThread.start();
                String newIp = null;
                int newPort = -1;
                while(newIp==null || newPort <= 0){
                    newIp = handler.getIp();
                    newPort = handler.getPort();
                    //System.out.println("a");
                }
                //System.out.println("newport at first is " + newPort);
                broadcast(newIp,newPort);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void broadcast(String newIp, int newPort) throws IOException {
        //System.out.println("size is " +(handlers.size()-1) );
        for(int i = 0; i<handlers.size()-1;i++){
            handlers.get(i).connect(newIp,newPort);
            //System.out.println("broadcast newport is " + newPort);
        }
    }
    public static int getPortNumber() {//help, it doesn't remember when i increase getport it returns starting value + 1 always
        //getPort++;
        //return getPort;
        Random r = new Random();
        return (r.nextInt()%3000)+6000;
    }}

class P2PClientHandler implements Runnable {
    private final Socket clientSocket;
    private ArrayList<String> connectedPeers;
    private PrintWriter out;
    public P2PClientHandler(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        this.out = new PrintWriter(clientSocket.getOutputStream(), true);
    }
    private String peerIp;
    private int peerPort;

    public void run() {
        boolean finished = false;
        boolean first = true;
        connectedPeers = new ArrayList<>(0);
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                //PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                String input;
                String[] inputs;
                // Handle client connection here
                // Example: read data from client and send response
                //BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                //might need to sign message as peer discovery protocol
                while(true) {
                    input = in.readLine();
                    if(first && input!=null){
                        inputs = input.split(" ");
                        connectedPeers.add(inputs[0]);
                        this.peerIp = inputs[0];
                        this.peerPort = Integer.parseInt(inputs[1]);
                        //System.out.println("we change newport to " + Integer.parseInt(inputs[1]));
                        first = false;
                        //connect(inputs[0],Integer.valueOf(inputs[1]));
                        out.println("fala broo");
                    }
                    //either commands like these or protocols like in the example??
                    else if(input.contains("//invite")){
                        int randIndex;
                        do {
                            randIndex = new Random().nextInt(P2PGameServer.handlers.size());//careful whether its this or this -1
                        }while (P2PGameServer.handlers.get(randIndex)==this);
                        P2PClientHandler contestant = P2PGameServer.handlers.get(randIndex);
                        //i may need IDs of all the peers or maybe a way to know which one is which in each peers manager.
                        String message = "//offer "+ contestant.peerIp+" "+contestant.peerPort;
                        out.println(message);
                        //out.println("invitation sent");
                    }
                    else{
                        out.println("fala broo");
                    }
                    System.out.println("Received input from client: " + input);
                    System.out.println();
                    //clientSocket.getOutputStream().write("Hello, client!".getBytes());
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }

    public String getIp() {
        return this.peerIp;
    }
    public int getPort() {
        return this.peerPort;
    }
    public void connect(String newIp, int port) throws IOException {
        //might need to sign message as connection protocol?
        out.println("//info "+newIp+" "+port);
    }
    public void send(String message){
        //might need to sign message with an invitation offer protocol?
        out.println(message);
    }

}

