import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PeerManager {
    public Map<String,Socket> connectionList; // List to store peers
    //public ArrayList<Socket> connectionList;
    private ExecutorService executorService;
    public PeerManager() {
        //this.connectionList = new ArrayList<>();
        this.connectionList = new HashMap<>();
        //Map or HashMap??
        this.executorService = Executors.newCachedThreadPool();
    }

    public synchronized void addPeer(String key, Socket peer) throws IOException {
        /*if (!connectionList.contains(peer)) {
            connectionList.add(peer);
            /*executorService.submit(()->{
                try {
                    Socket connect = new Socket(peer.ip,P2PGameServer.PORT);

                    //peerList.add(connPeer);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }(ovde isto staj * /)

            });
            System.out.println("Peer " + peer + " added.");
            PrintWriter out = new PrintWriter(peer.getOutputStream(), true);
            out.println("ayee wsgood gang");
        } else {
            System.out.println("Peer " + peer + " already exists.");
        }*/
        if(!connectionList.containsKey(key)){
            connectionList.put(key,peer);
            System.out.println("Peer " + peer + " added.");
            PrintWriter out = new PrintWriter(peer.getOutputStream(), true);
            out.println("ayee wsgood gang");
        }
        else{
            System.out.println("Peer " + peer + " already exists.");
        }
    }

    /*public synchronized ArrayList<Socket> getAllPeers() {
        return connectionList; // Return a copy of the peerList
    }*/
    public synchronized Map<String,Socket> getAllPeers(){
        return connectionList;
    }
}
