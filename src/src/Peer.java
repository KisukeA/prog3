import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Peer {
    public static int playerNumber=-1;
    private ArrayList<Peer> connectedPeers;
    public static Object lock = new Object();
    private static PeerManager pm = new PeerManager();
    public static InetAddress ip;
    private BufferedReader in;
    private PrintWriter out;
    public static boolean sign = false;
    public static boolean gameOn = false;
    public static List<String> inputQ = new ArrayList<>();
    public static TicTacToe game;
    public static boolean goesFirst = false;
    public static boolean waitingAnswer = false;
    private static Crypto crypto;
    public static KeyPair kp;

    //public Peer(PeerManager peerManager) throws IOException {
        //this.pm = peerManager;
        //this.ip=InetAddress.getLocalHost();
        //in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        //out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
    //}
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        crypto = new Crypto(".");
        kp = crypto.loadKeyPair(".");
        //game = new TicTacToe();
        boolean finished = false;
        ip = InetAddress.getLocalHost();
        boolean first = true;
        //all the peers automatically connect to the trusted??
        try {
            Socket socket = new Socket("localhost", 8888);
            System.out.println("Connected to server.");

            // create output stream to send message to server
            OutputStream os = socket.getOutputStream();
            PrintWriter pw = new PrintWriter(os, true);
            BufferedReader r;
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String input;

            int portnumber = P2PGameServer.getPortNumber();

            PeerListener peerListener = new PeerListener(portnumber, lock);
            Thread listener = new Thread(peerListener);
            listener.start();
            //not sure whether to pass pm or use it globally, what if something changes is there any errors?
            InputHandler inputHandler = new InputHandler(br,pw,portnumber,pm);
            Thread inputHandle = new Thread(inputHandler);
            inputHandle.start();
            r = new BufferedReader(new InputStreamReader(System.in));
            //imma try doing this only once and closing the streams let's see how it goes
            //still the same problem without this while loop
            while(true) {//here we handle only inputs from the user
                Thread.sleep(50);

                // i have to somehow block this or close this when 2 peers are communicating
                //maybe some global variable for active communication between peers
                //PARALEL PROGRAMING!!!
                /*if(!activeCommPeers){
                    //Thread.sleep(50);
                    String res = r.readLine();
                    pw.println(res);
                }
                else{
                    r.close();
                */
                //it doesnt work like this
                //while(cont){}

                while(waitingAnswer){
                    Thread.sleep(1000);
                    System.out.println("ajm hir");
                }
                //because at the start it already comes here and its blocked waiting for an input
                synchronized (lock){
                    String res="";
                    if(gameOn&&game.winner<0){//not like this
                        game.Xmoved = false;
                        game.Omoved = false;
                        while ((playerNumber-1) != game.playerXO){//i have to branch here somehow, signing the messages probably
                                System.out.println(playerNumber+" and turn is "+game.playerXO);
                                    String moves = game.br.readLine();
                                    String[] move = moves.split(" ");
                                    game.move(Integer.parseInt(move[0]),Integer.parseInt(move[1]),move[2]);
                                    game.gameState();
                                    if (game.winner>=0){
                                        endScreen(game.winner);
                                    }
                                    System.out.println("winner is: "+game.winner);

                        }
                        System.out.println("or maybe here??");
                        res = r.readLine();
                    }
                    else{
                        System.out.println("am i here?");
                        res = r.readLine();
                        if(res.contains("//invite")){
                            waitingAnswer = true;
                        }
                    }
                String attach = "";
                //if false that means we talking to the server
                //else we tryna respond to a peer so we do something else
                if(sign){
                    attach = "//✧ ";
                }
                if(gameOn){
                    attach = "//⌔ ";
                }
                res = attach+res;
                System.out.println(sign);
                System.out.println(res);
                //i have to find a way to remove this somehow when i get to the answer part
                //not sure if i really need this lock

                    //here is where i will need protocol messages, or some signs or special characters
                    //but most probably it will be a Message object like the example from Prof.
                    if (res.startsWith("//✧ ")){
                        //cont = true;
                        res = res.substring(res.indexOf(" ")+1);
                        while (!res.equalsIgnoreCase("y")
                                && !res.equalsIgnoreCase("yes")
                                && !res.equalsIgnoreCase("n")
                                && !res.equalsIgnoreCase("no")){
                            System.out.println("Try again: ");
                            res = r.readLine();
                        }
                        inputQ.add(res);
                        sign = false;
                        System.out.println("go smenav "+" ev");
                    }
                    else if(res.startsWith("//⌔")){
                        //i should also check for exceptions if it's not a number
                        res = res.substring(res.indexOf(" ")+1);
                        int x=0;
                        int y=0;

                        boolean invalid = true;
                        int intres = 0;
                        while(invalid){
                            try{
                                intres = Integer.parseInt(res);
                                x = intres/10;
                                y = intres%10;
                                invalid = false;//we change it so we get out
                            }
                            catch (NumberFormatException e){
                                System.out.println("Please input an integer: ");
                                res = r.readLine();
                                invalid = true;//unless we get here and we loop again
                            }
                        }
                        while(x<1 || x>3 || y<1 || y>3 || !game.isValid(x-1,y-1)){
                            //System.out.println(game.isValid(x-1,y-1));
                            System.out.println("Please enter a (valid) move again: ");
                            System.out.println("x: "+x +", y: "+y+" aand "+game.map[x-1][y-1]);
                            res = r.readLine();
                            invalid = true;
                            while(invalid){
                                try{
                                    x = Integer.parseInt(res)/10;
                                    y = Integer.parseInt(res)%10;
                                    invalid = false;//we change it so we get out
                                }
                                catch (NumberFormatException e){
                                    System.out.println("Please input an integer: ");
                                    res = r.readLine();
                                    invalid = true;//unless we get here and we loop again
                                }
                            }
                        }
                        System.out.println("playnum "+(playerNumber-1));
                        System.out.println(game.playerXO);

                        //now we have the player move//validate(game,x,y,...);//now make the move//if game over gameOn = false
                        if(game.playerXO==0){
                            game.move(x-1, y-1, "⨉");
                            game.pw.println((x-1)+" "+(y-1)+" "+"⨉");
                            game.gameState();
                            if (game.winner>=0){
                                endScreen(game.winner);
                                game.Omoved = true;
                            }
                            System.out.println("winner is: "+game.winner);
                            //game.playerXO = (game.playerXO+1)%2;
                        }
                        else{
                            game.move(x-1, y-1, "〇");
                            game.pw.println((x-1)+" "+(y-1)+" "+"〇");
                            game.gameState();
                            if (game.winner>=0){
                                endScreen(game.winner);
                                game.Xmoved = true;
                            }
                            System.out.println("winner is: "+game.winner);
                            //game.playerXO = (game.playerXO+1)%2;
                        }
                        while(!game.Xmoved || !game.Omoved){//guy is looping here so he never gets the message of our move
                            //probably have to listen here??
                                String moves= game.br.readLine();
                                String newMoves[] = moves.split(" ");
                                System.out.println("ne trebit da sum ovde");
                                System.out.println(newMoves[2]);
                                game.move(Integer.parseInt(newMoves[0]),Integer.parseInt(newMoves[1]),newMoves[2]);
                                game.gameState();
                                if (game.winner>=0){
                                    endScreen(game.winner);
                                }
                                System.out.println("winner is: "+game.winner);
                                System.out.println(game.Omoved +" "+game.Xmoved);
                        }
                    }
                    else {
                        pw.println(res);
                    }
                }
                //r.close();
            }
            // close the streams and socket
            //pw.close();
            //br.close();
            //socket.close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void endScreen(int winner) {
        gameOn = false;
        if(winner == 1){
            System.out.println("Player 1 won!");
        }
        else if(winner == 2){
            System.out.println("Player 2 won!");
        }
        else{
            System.out.println("It's a draw haha");
        }
    }

    private static void validate(TicTacToe game, int x, int y) {

    }

    public static void connectToPeer(String key, String ip, int port) throws IOException {
        pm.addPeer(key,new Socket(ip,port));
    }
    public void send(String s){
        out.println(s);
    }
    public static void attachSign(){
        sign = true;
    }


}
class PeerListener implements Runnable{
    private ServerSocket socketConn;
    boolean done = false;
    private BufferedReader in;
    private PrintWriter out;
    private final Object lock;
    public PeerListener(int port, Object lock) throws IOException {
        this.socketConn = new ServerSocket(port);
        this.lock = lock;
    }
    @Override
    public void run() {
        while(!done){
            try {
                Socket socket = socketConn.accept();
                //should I make a thread maybe for every connection i accept, but i think that's too much
                System.out.println("aceptnav socket");
                System.out.println(socket);
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()),true);
                String answer="";
                //System.out.println("alo bree");
                String input = in.readLine();
                if(!input.contains("//answer")){
                    System.out.println("so izrembla ovaj: " + input);
                }
                else{
                    //Peer.activeCommPeers = true;
                    //Thread.sleep(30);
                    BufferedReader scan;
                    //synchronized (lock) {
                        //scan = new BufferedReader(new InputStreamReader(System.in));
                        //}maybe this is the end of the CS
                        System.out.println(input.substring(input.indexOf(" ") + 1));
                        System.out.println(in.readLine());//i put a \n which makes a newline and i forgot the reader reads only 1 line
                        //for some reason, i guess because the stream of output to the server is still running
                            //whatever i type here gets taken there (line 50)
                            //Peer.r
                            //answer = scan.nextLine();
                            //maybe here i need to put a sign to answer that it's an answer and send it to the main handler
                            //probably gonna need to call a function to retreive this answer string also
                            Peer.attachSign();
                            System.out.println("1: "+answer);
                            //wait until user enters something
                            synchronized (lock){
                                answer = Peer.inputQ.get(Peer.inputQ.size()-1);
                                Peer.inputQ.remove(Peer.inputQ.size()-1);
                                System.out.println("2: "+answer);
                                if(answer.equalsIgnoreCase("y") || answer.equalsIgnoreCase("yes")){
                                    Peer.gameOn = true;//i should also here find a wayt yo tell the server that im in game now
                                    //so that it removes me from the possible matchmaking list for now
                                    TicTacToe game = new TicTacToe(out,in);
                                    Peer.game = game;
                                    System.out.println(Peer.game);
                                    out.println(answer);
                                    //ObjectOutputStream objectOut = new ObjectOutputStream(socket.getOutputStream());
                                    //objectOut.writeObject(game);
                                    //this gets sent to the stream of the server not the stream of the peer???
                                    int playerNum = Integer.parseInt(in.readLine());
                                    Peer.playerNumber = playerNum;
                                    System.out.println(playerNum);
                                }
                                //we need to make sure to set gameOn before the other thread reads line again and does stuff we don't want.
                                else{
                                    out.println(answer);
                                }
                            }
                            //System.out.println(answer);
                            //answer = scan.readLine();
                            //System.out.println("so ke odgovoris");
                            //should i maybe close streams to the server first?
;                    //}//or this is the end??
                    //Peer.activeCommPeers = false;
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
class InputHandler implements Runnable{
    //here I handle all the necessary inputs (calls) from the server
    private BufferedReader scanner;
    private boolean first = true;
    private int port;
    //private Scanner r = new Scanner(System.in);
    private PrintWriter writer;
    private PeerManager peerManager;
    public InputHandler(BufferedReader br, PrintWriter pw, int port, PeerManager pm) {
        this.writer = pw;
        this.scanner = br;
        this.port = port;
        this.peerManager = pm;
    }

    @Override
    public void run() {
        while(true) {
            String input;
            if(first){//if it's the first time we do this here since it's automatic, the user doesn't input this
                try {
                    input = InetAddress.getLocalHost().getHostAddress().toString();
                } catch (UnknownHostException e) {
                    throw new RuntimeException(e);
                }
                input += " "+port;
                writer.println(input);
                first = false;
            }
            else{
            //maybe need an input handler seperate
            // read response from server and print it
                try {
                    input = scanner.readLine();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("Response from server: " + input);
            //maybe put //info here as a command?
            if(input.contains("//info")){
                System.out.println("are we ever here?");
                String[] inputs = input.split(" ");
                System.out.println(inputs[1]);
                System.out.println(Integer.parseInt(inputs[2]));
                try {
                    Peer.connectToPeer(input.substring(input.indexOf(" ")+1),inputs[1],Integer.parseInt(inputs[2]));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            else if(input.contains("//offer")){//this means u offer an invite to the peer im sending u
                String[] inputs = input.split(" ");
                //take the key of the peer in our hash map
                String peerId = inputs[1]+" "+inputs[2];
                //should I close the socket and connect again or find another way to communicate
                //but then the automatic connection when e new peer enters would be useless, since i can do it like this here
                // if i need to connect, i will just call the function connectToPeer here ??
                Socket gameConnection = peerManager.connectionList.get(peerId);
                System.out.println(gameConnection);
                if(gameConnection!=null){
                    try {//dont forget to do everything here as below!!!!!!!!!!@@@@@@
                        System.out.println("it wasn't null");
                        //this is the only way i can think of right now, help ??
                        gameConnection = null;
                        gameConnection = new Socket(inputs[1],Integer.parseInt(inputs[2]));
                        PrintWriter printWriter = new PrintWriter(gameConnection.getOutputStream(),true);
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(gameConnection.getInputStream()));
                        printWriter.println("//answer "+peerId+" sent an invitation for a game.\nType \"Y\"/\"y\"/\"yes\"/\"Yes\" if you accept and \"n\"/\"No\"/\"N\"/\"no\" if you decline the offer.");
                        String acceptOrDecline = bufferedReader.readLine();
                        System.out.println("Brodie said: " +acceptOrDecline);
                        System.out.println("finished waiting1");
                        if(acceptOrDecline.equalsIgnoreCase("y")||acceptOrDecline.equalsIgnoreCase("yes")){
                            Peer.gameOn = true;
                            Peer.waitingAnswer = false;
                            //Game game = new Game();
                            //ObjectInputStream objectIn = new ObjectInputStream(gameConnection.getInputStream());
                            //TicTacToe g = (TicTacToe) objectIn.readObject();
                            Peer.game = new TicTacToe(printWriter,bufferedReader);
                            //System.out.println(Peer.game);
                            Random rand = new Random();
                            int playerNum = rand.nextInt(2) + 1;//the number of the player (player1 or player2)
                            Peer.playerNumber = playerNum;
                            System.out.println(playerNum);
                            printWriter.println(3-playerNum);
                            //generating nonce for the seed
                            int nonce = rand.nextInt();
                        }
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
                else {
                    try {
                        System.out.println("it was null");
                        gameConnection = new Socket(inputs[1],Integer.parseInt(inputs[2]));
                        PrintWriter printWriter = new PrintWriter(gameConnection.getOutputStream(),true);
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(gameConnection.getInputStream()));
                        printWriter.println("//answer "+peerId+ " sent an invitation for a game.\nType \"Y\"/\"y\"/\"yes\"/\"Yes\" if you accept and \"n\"/\"No\"/\"N\"/\"no\" if you decline the offer.");
                        String acceptOrDecline = bufferedReader.readLine();
                        System.out.println("Brodie said: " +acceptOrDecline);
                        System.out.println("finished waiting");
                        if(acceptOrDecline.equalsIgnoreCase("y")||acceptOrDecline.equalsIgnoreCase("yes")){
                            Peer.gameOn = true;
                            Peer.waitingAnswer = false;
                            //Game game = new Game();
                            //ObjectInputStream objectIn = new ObjectInputStream(gameConnection.getInputStream());
                            //TicTacToe g = (TicTacToe) objectIn.readObject();
                            Peer.game = new TicTacToe(printWriter,bufferedReader);
                            //System.out.println(Peer.game);
                            Random rand = new Random();
                            int playerNum = rand.nextInt(2) + 1;//the number of the player (player1 or player2)
                            Peer.playerNumber = playerNum;
                            System.out.println(playerNum);
                            printWriter.println(3-playerNum);//the other players number
                            //now the fun part begins
                            //Thread.sleep(10);
                            //i should also tell the server that im in game now
                            //so that it removes me from the possible matchmaking list for now
                            //we will eventually have to disable gameOn when the game finishes but it's still to early for that
                        }
                        else{
                            System.out.println("Sorry, your invitation has been declined\nSend another one if you still want to play.");
                        }
                        //great now it's fixed, but still i have to make it process only one output
                        // i have to somehow close the output stream to the server first or do something about it
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            }
        //UBO E ZASSEA AMA POSLE PRVIOT POTEG SAM SI IGRAT PRVIOV VTORIOV NEMOJT DA VRATTIT XD??
        }
    }//RABOTI NA ZAVRSETOKON MALU
}
//i will make a list of disabled for invitation and each time a game starts i will inform the server of the 2 playing
//so he could put it in that list, and when it chooses for invitation it will omit those peers
//when they finish a game it will inform the server to remove them from that list, but that's still too far off rn.